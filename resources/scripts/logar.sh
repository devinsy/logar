#!/bin/bash

# Java check.
javaCheck=`which java`
if [[ "$javaCheck" =~ ^/.* ]]; then
	#echo "Java requirement............... OK"
    java -jar "$(dirname "$0")"/logar.jar $@
else
	echo "Java requirement............... MISSING"
fi
