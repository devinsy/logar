/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple key value database.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.devinsy.logar.app.LogarException;
import fr.devinsy.logar.app.anonymizer.Ipv4Generator;

/**
 * The Class VisitCountersTest.
 */
public class TestFoo
{
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestFoo.class);

    /**
     * Test 01.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void test01() throws Exception
    {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void test02() throws Exception
    {
        System.out.println(" 7: " + Ipv4Generator.random(7));
        System.out.println(" 8: " + Ipv4Generator.random(8));
        System.out.println(" 9: " + Ipv4Generator.random(9));
        System.out.println("10: " + Ipv4Generator.random(10));
        System.out.println("11: " + Ipv4Generator.random(11));
        System.out.println("12: " + Ipv4Generator.random(12));
        System.out.println("13: " + Ipv4Generator.random(13));
        System.out.println("14: " + Ipv4Generator.random(14));
        System.out.println("15: " + Ipv4Generator.random(15));
    }

    /**
     * After class.
     * 
     * @throws LogarException
     *             the Logar exception
     */
    @AfterClass
    public static void afterClass() throws LogarException
    {
    }

    /**
     * Before class.
     * 
     * @throws LogarException
     *             the Logar exception
     */
    @BeforeClass
    public static void beforeClass() throws LogarException
    {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.DEBUG);
    }
}
