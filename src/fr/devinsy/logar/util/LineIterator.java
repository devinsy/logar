/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class LineIterator.
 */
public class LineIterator
{
    private static Logger logger = LoggerFactory.getLogger(LineIterator.class);

    public static final String DEFAULT_CHARSET_NAME = "UTF-8";

    private BufferedReader in;
    private String nextLine;
    private boolean ready;

    /**
     * Instantiates a new http log iterator.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public LineIterator(final File source) throws IOException
    {
        if (source.getName().endsWith(".gz"))
        {
            this.in = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(source))));
        }
        else
        {
            this.in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));
        }

        this.nextLine = null;
        this.ready = false;
    }

    /**
     * Close.
     */
    public void close()
    {
        IOUtils.closeQuietly(this.in);
    }

    /**
     * Checks for next.
     *
     * @return true, if successful
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public boolean hasNext() throws IOException
    {
        boolean result;

        setReady();

        if (this.nextLine == null)
        {
            result = false;
            close();
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Next.
     *
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public String next() throws IOException
    {
        String result;

        setReady();

        result = this.nextLine;
        this.ready = false;

        //
        return result;
    }

    /**
     * Sets the ready.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void setReady() throws IOException
    {
        if (!this.ready)
        {
            this.nextLine = this.in.readLine();
            this.ready = true;
        }
    }
}
