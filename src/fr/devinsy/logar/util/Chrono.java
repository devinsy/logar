/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.util;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * The Class Chrono.
 */
public class Chrono
{
    private LocalDateTime start;

    /**
     * Instantiates a new time keeper.
     */
    public Chrono()
    {
        reset();
    }

    /**
     * Duration.
     *
     * @return the long
     */
    public long duration()
    {
        long result;

        result = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - this.start.toEpochSecond(ZoneOffset.UTC);

        //
        return result;
    }

    /**
     * Format.
     *
     * @return the string
     */
    public String format()
    {
        String result;

        if (this.start == null)
        {
            result = "n/a";
        }
        else
        {
            LocalDateTime end = LocalDateTime.now();
            Duration duration = Duration.between(this.start, end);

            result = String.format("%02d:%02d:%02d", duration.getSeconds() / 60 / 60, duration.getSeconds() / 60, duration.getSeconds() % 60);
        }

        //
        return result;
    }

    /**
     * Reset.
     */
    public void reset()
    {
        this.start = null;
    }

    /**
     * Start.
     *
     * @return the chrono
     */
    public Chrono start()
    {
        Chrono result;

        this.start = LocalDateTime.now();

        result = this;

        //
        return result;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        result = format();

        //
        return result;
    }
}
