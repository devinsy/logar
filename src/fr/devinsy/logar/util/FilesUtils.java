/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.util;

import java.io.File;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class FilesUtils.
 */
public class FilesUtils
{

    /**
     * Instantiates a new files utils.
     */
    private FilesUtils()
    {
    }

    /**
     * List recursively.
     *
     * @param source
     *            the source
     * @return the files
     */
    public static Files listRecursively(final File source)
    {
        Files result;

        result = new Files();
        if ((source != null) && (source.exists()))
        {
            if (source.isFile())
            {
                result.add(source);
            }
            else
            {
                for (File file : source.listFiles())
                {
                    if (file.isDirectory())
                    {
                        result.addAll(listRecursively(file));
                    }
                    else
                    {
                        result.add(file);
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Search recursively.
     *
     * @param source
     *            the source
     * @param regex
     *            the regex
     * @return the files
     */
    public static Files search(final File source, final String regex)
    {
        Files result;

        result = new Files();

        Pattern pattern = Pattern.compile(regex);

        Files full = listRecursively(source);
        for (File file : full)
        {
            if (pattern.matcher(file.getName()).matches())
            {
                result.add(file);
            }
        }

        //
        return result;
    }

    /**
     * List recursively.
     *
     * @param source
     *            the source
     * @param extensions
     *            the extensions
     * @return the files
     */
    public static Files searchEndingWith(final File source, final String... extensions)
    {
        Files result;

        result = new Files();

        Files full = listRecursively(source);
        for (File file : full)
        {
            if (StringUtils.endsWithAny(file.getName(), extensions))
            {
                result.add(file);
            }
        }

        //
        return result;
    }
}
