/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserAgentStat.
 */
public final class UserAgent
{
    private static Logger logger = LoggerFactory.getLogger(UserAgent.class);

    private String value;
    private long count;
    private long ipLinkCount;

    /**
     * Instantiates a new user agent.
     *
     * @param userAgent
     *            the user agent
     */
    public UserAgent(final String userAgent)
    {
        this.value = userAgent;
        this.count = 0;
        this.ipLinkCount = 0;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public long getCount()
    {
        return this.count;
    }

    /**
     * Gets the ip link count.
     *
     * @return the ip link count
     */
    public long getIpLinkCount()
    {
        return this.ipLinkCount;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue()
    {
        return this.value;
    }

    /**
     * Inc.
     */
    public void inc()
    {
        this.count += 1;
    }

    /**
     * Sets the ip link count.
     *
     * @param ipLinkCount
     *            the new ip link count
     */
    public void setIpLinkCount(final long ipLinkCount)
    {
        this.ipLinkCount = ipLinkCount;
    }

}