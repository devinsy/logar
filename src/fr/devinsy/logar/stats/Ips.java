/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.stats;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class IpStats.
 */
public final class Ips extends HashMap<String, Ip>
{
    private static final long serialVersionUID = -8411746796941831991L;

    private static Logger logger = LoggerFactory.getLogger(Ips.class);

    /**
     * Instantiates a new ip stats.
     */
    public Ips()
    {
        super();
    }

    /**
     * Put.
     *
     * @param ip
     *            the ip
     */
    public void put(final String ip)
    {
        Ip stat = get(ip);
        if (stat == null)
        {
            stat = new Ip(ip);
            this.put(ip, stat);
        }

        stat.inc();
    }
}