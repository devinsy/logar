/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.stats;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.logar.app.log.Log;
import fr.devinsy.logar.util.Chrono;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class UserAgentStator.
 */
public final class UserAgentStator
{
    private static Logger logger = LoggerFactory.getLogger(UserAgentStator.class);

    private long logCount;
    private Ips ips;
    private UserAgents userAgents;
    private IpUserAgents ipUserAgents;

    /**
     * Instantiates a new user agent stator.
     */
    public UserAgentStator()
    {
        this.logCount = 0;
        this.ips = new Ips();
        this.userAgents = new UserAgents();
        this.ipUserAgents = new IpUserAgents();
    }

    /**
     * Compute ip link count for user agent.
     */
    public void computeIpLinkCountForUserAgent()
    {
        int index = 0;
        Chrono chrono = new Chrono().start();
        long lastDuration = 0;
        for (UserAgent userAgent : this.userAgents.values())
        {
            index += 1;

            if ((chrono.duration() % 60 == 0) && (chrono.duration() != lastDuration))
            {
                lastDuration = chrono.duration();
                System.out.println(chrono.format() + " IpLinkCount " + index + "/" + this.userAgents.size());
            }

            long count = this.ipUserAgents.countByUserAgent(userAgent.getValue());

            userAgent.setIpLinkCount(count);
        }
    }

    /**
     * Gets the ips.
     *
     * @return the ips
     */
    public Ips getIps()
    {
        return this.ips;
    }

    /**
     * Gets the ip user agents.
     *
     * @return the ip user agents
     */
    public IpUserAgents getIpUserAgents()
    {
        return this.ipUserAgents;
    }

    /**
     * Gets the log count.
     *
     * @return the log count
     */
    public long getLogCount()
    {
        return this.logCount;
    }

    /**
     * Gets the user agents.
     *
     * @return the user agents
     */
    public UserAgents getUserAgents()
    {
        return this.userAgents;
    }

    /**
     * Adds the log.
     *
     * @param log
     *            the log
     */
    public void putLog(final Log log)
    {
        String userAgent = log.getUserAgent().trim();

        this.logCount += 1;
        this.ips.put(log.getIp());
        this.userAgents.put(userAgent);
        this.ipUserAgents.put(log.getIp(), userAgent);
    }

    /**
     * Save ip list.
     *
     * @param target
     *            the target
     */
    public void saveIpList(final File target)
    {
        PrintWriter out = null;
        try
        {
            out = new PrintWriter(new FileOutputStream(target));

            for (Ip ip : this.ips.values())
            {
                out.println(ip.getValue());
            }
        }
        catch (FileNotFoundException exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Save ip user agent list.
     *
     * @param target
     *            the target
     */
    public void saveIpUserAgentList(final File target)
    {
        PrintWriter out = null;
        try
        {
            out = new PrintWriter(new FileOutputStream(target));

            for (IpUserAgent userAgent : this.ipUserAgents)
            {
                out.println(userAgent.getIp() + " " + userAgent.getUserAgent());
            }
        }
        catch (FileNotFoundException exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Save user agent link count.
     *
     * @param target
     *            the target
     */
    public void saveUserAgentLinkCount(final File target)
    {
        PrintWriter out = null;
        try
        {
            out = new PrintWriter(new FileOutputStream(target));
            for (UserAgent userAgent : this.userAgents.values())
            {
                out.println(userAgent.getIpLinkCount() + " " + userAgent.getCount() + " " + userAgent.getValue());
            }
        }
        catch (FileNotFoundException exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Save user agent list.
     *
     * @param target
     *            the target
     */
    public void saveUserAgentList(final File target)
    {
        PrintWriter out = null;
        try
        {
            out = new PrintWriter(new FileOutputStream(target));

            for (UserAgent userAgent : this.userAgents.values())
            {
                out.println(userAgent.getValue());
            }
        }
        catch (FileNotFoundException exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Shrink.
     */
    public void shrink()
    {
        String[] tokens = { "android", "apple", "chrome", "iphone", "linux", "mac", "mozilla", "opera", "safari", "thunderbird" };

        //
        UserAgents userAgentBis = new UserAgents();
        for (UserAgent userAgent : this.userAgents.values())
        {
            if (StringsUtils.containsAnyIgnoreCase(userAgent.getValue(), tokens))
            {
                userAgentBis.put(userAgent);
            }
        }
        this.userAgents.clear();
        this.userAgents = userAgentBis;

        //
        IpUserAgents ipUserAgentBis = new IpUserAgents();
        for (IpUserAgent ipUserAgent : this.ipUserAgents)
        {
            if (StringsUtils.containsAnyIgnoreCase(ipUserAgent.getUserAgent(), tokens))
            {
                ipUserAgentBis.put(ipUserAgent);
            }
        }
        this.ipUserAgents.clear();
        this.ipUserAgents = ipUserAgentBis;
    }
}