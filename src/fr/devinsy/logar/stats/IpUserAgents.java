/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.stats;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class IpUserAgentStats.
 */
public final class IpUserAgents implements Iterable<IpUserAgent>
{
    private static final long serialVersionUID = -3011532898476944152L;

    private static Logger logger = LoggerFactory.getLogger(IpUserAgents.class);

    private HashMap<String, IpUserAgent> data;

    /**
     * Instantiates a new ip user agent stats.
     */
    public IpUserAgents()
    {
        this.data = new HashMap<String, IpUserAgent>();
    }

    /**
     * Clear.
     */
    public void clear()
    {
        this.data.clear();
    }

    /**
     * Count by user agent.
     *
     * @param userAgent
     *            the user agent
     * @return the int
     */
    public int countByUserAgent(final String userAgent)
    {
        int result;

        result = 0;

        for (IpUserAgent ipUserAgent : this.data.values())
        {
            if (StringUtils.equals(ipUserAgent.getUserAgent(), userAgent))
            {
                result += 1;
            }
        }

        //
        return result;
    }

    /**
     * Gets the by user agent.
     *
     * @param userAgent
     *            the user agent
     * @return the by user agent
     */
    public IpUserAgentList getByUserAgent(final String userAgent)
    {
        IpUserAgentList result;

        result = new IpUserAgentList();

        for (IpUserAgent ipUserAgent : this.data.values())
        {
            if (StringUtils.equals(ipUserAgent.getUserAgent(), userAgent))
            {
                result.add(ipUserAgent);
            }
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<IpUserAgent> iterator()
    {
        Iterator<IpUserAgent> result;

        result = this.data.values().iterator();

        //
        return result;
    }

    /**
     * Put.
     *
     * @param ipUserAgent
     *            the ip user agent
     */
    public void put(final IpUserAgent ipUserAgent)
    {
        this.data.put(ipUserAgent.getIp() + ipUserAgent.getUserAgent(), ipUserAgent);
    }

    /**
     * Put.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     */
    public void put(final String ip, final String userAgent)
    {
        IpUserAgent stat = this.data.get(ip + userAgent);
        if (stat == null)
        {
            stat = new IpUserAgent(ip, userAgent);
            this.data.put(ip + userAgent, stat);
        }

        stat.inc();
    }

    /**
     * Size.
     *
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.data.size();

        //
        return result;
    }
}