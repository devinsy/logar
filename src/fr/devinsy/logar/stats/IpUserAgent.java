/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserAgentStat.
 */
public final class IpUserAgent
{
    private static Logger logger = LoggerFactory.getLogger(IpUserAgent.class);

    private String ip;
    private String userAgent;
    private long count;

    /**
     * Instantiates a new user agent stat.
     *
     * @param ip
     *            the ip
     * @param userAgent
     *            the user agent
     */
    public IpUserAgent(final String ip, final String userAgent)
    {
        this.ip = ip;
        this.userAgent = userAgent;
        this.count = 0;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public long getCount()
    {
        return this.count;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp()
    {
        return this.ip;
    }

    /**
     * Gets the user agent.
     *
     * @return the user agent
     */
    public String getUserAgent()
    {
        return this.userAgent;
    }

    /**
     * Inc.
     */
    public void inc()
    {
        this.count += 1;
    }

}