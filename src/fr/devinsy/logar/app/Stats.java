/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app;

import java.time.Duration;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.strings.StringList;

/**
 * The Class Counter.
 */
public final class Stats
{
    private static Logger logger = LoggerFactory.getLogger(Stats.class);

    private LocalDateTime start;
    private LocalDateTime end;

    private int lineCount;
    private int successLineCount;
    private int errorLineCount;
    private int archivedLineCount;

    private int fileCount;
    private int successFileCount;
    private int errorFileCount;

    /**
     * Instantiates a new stats.
     */
    public Stats()
    {
        this.start = LocalDateTime.now();
        this.end = null;

        this.lineCount = 0;
        this.successLineCount = 0;
        this.errorLineCount = 0;

        this.fileCount = 0;
        this.successFileCount = 0;
        this.errorFileCount = 0;

        this.archivedLineCount = 0;
    }

    /**
     * Duration.
     *
     * @return the long
     */
    public long getDuration()
    {
        long result;

        if (this.start == null)
        {
            result = 0;
        }
        else if (this.end == null)
        {
            result = Duration.between(this.start, LocalDateTime.now()).getSeconds();
        }
        else
        {
            result = Duration.between(this.start, this.end).getSeconds();
        }

        //
        return result;
    }

    /**
     * Gets the human duration.
     *
     * @return the human duration
     */
    public String getDurationForHuman()
    {
        String result;

        long duration = getDuration();
        long seconds = duration % 60;
        long minutes = duration / 60;
        result = String.format("00:%02d:%02d", minutes, seconds);

        //
        return result;
    }

    /**
     * Gets the human speed.
     *
     * @return the human speed
     */
    public String getSpeedForHuman()
    {
        String result;

        long duration = getDuration();
        if (duration > 0)
        {
            result = String.valueOf(this.lineCount / duration);
        }
        else
        {
            result = "n/a";
        }

        //
        return result;
    }

    /**
     * Inc archived line count.
     */
    public void incArchivedLineCount()
    {
        this.archivedLineCount += 1;
    }

    /**
     * Inc error file count.
     */
    public void incErrorFileCount()
    {
        this.errorFileCount += 1;
    }

    /**
     * Inc error line count.
     */
    public void incErrorLineCount()
    {
        this.errorLineCount += 1;
    }

    /**
     * Inc file count.
     */
    public void incFileCount()
    {
        this.fileCount += 1;
    }

    /**
     * Inc line count.
     */
    public void incLineCount()
    {
        this.lineCount += 1;
    }

    /**
     * Inc success file count.
     */
    public void incSuccessFileCount()
    {
        this.successFileCount += 1;
    }

    /**
     * Inc success line count.
     */
    public void incSuccessLineCount()
    {
        this.successLineCount += 1;
    }

    /**
     * Start.
     */
    public void start()
    {
        this.start = LocalDateTime.now();
    }

    /**
     * Stop.
     */
    public void stop()
    {
        this.end = LocalDateTime.now();
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        StringList lines = new StringList();

        lines.appendln("Lignes :");
        lines.append("  - traitées  : ").appendln(this.lineCount);
        lines.append("  - succès    : ").appendln(this.successLineCount);
        lines.append("  - erreurs   : ").appendln(this.errorLineCount);
        lines.append("  - archivées : ").appendln(this.archivedLineCount);

        lines.appendln("Fichiers :");
        lines.append("  - traités  : ").appendln(this.fileCount);
        lines.append("  - succès   : ").appendln(this.successFileCount);
        lines.append("  - erreurs  : ").appendln(this.errorFileCount);

        lines.appendln("Temps :");
        lines.append("  - durée    : ").appendln(getDurationForHuman());
        lines.append("  - vitesse  : ").append(getSpeedForHuman()).append(" lignes/seconde");

        result = lines.toString();

        //
        return result;
    }
}
