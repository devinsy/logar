/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class LogarUtils.
 */
public final class LogarUtils
{
    private static Logger logger = LoggerFactory.getLogger(LogarUtils.class);

    public static final String DEFAULT_CHARSET_NAME = "UTF-8";

    /**
     * Instantiates a new log tool utils.
     */
    private LogarUtils()
    {
    }

    /**
     * Sha 1 sum.
     *
     * @param file
     *            the file
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String sha1sum(final File file) throws IOException
    {
        String result;

        DigestInputStream digestStream = null;
        try
        {
            digestStream = new DigestInputStream(new FileInputStream(file), MessageDigest.getInstance("SHA-1"));
            digestStream.readAllBytes();
            result = DigestUtils.sha1Hex(digestStream.getMessageDigest().digest());
        }
        catch (NoSuchAlgorithmException e1)
        {
            throw new IOException("Impossible to get SHA-1 digester", e1);
        }
        finally
        {
            IOUtils.closeQuietly(digestStream);
        }

        //
        return result;
    }

    /**
     * To human.
     *
     * @param duration
     *            the duration
     * @return the string
     */
    public static String toHuman(final long duration)
    {
        String result;

        long seconds = duration % 60;
        long minutes = duration / 60;
        result = String.format("00:%02d:%02d", minutes, seconds);

        //
        return result;
    }
}
