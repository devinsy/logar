/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app;

import org.apache.commons.lang3.StringUtils;

/**
 * The Enum DryOption.
 */
public enum ExtractOption
{
    IP,
    DATETIME,
    USERAGENT;

    /**
     * Of.
     *
     * @param source
     *            the source
     * @return the extract option
     */
    public static ExtractOption of(final String source)
    {
        ExtractOption result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            String token = source.toLowerCase().trim();
            if (StringUtils.equals(token, "ip"))
            {
                result = IP;
            }
            else if (StringUtils.equals(token, "dateime"))
            {
                result = DATETIME;
            }
            else if (StringUtils.equals(token, "useragent"))
            {
                result = USERAGENT;
            }
            else
            {
                result = null;
            }
        }

        //
        return result;
    }
}
