/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.cmdexec.CmdExecException;
import fr.devinsy.cmdexec.CmdExecUtils;
import fr.devinsy.logar.app.log.parser.AccessLogParser;
import fr.devinsy.logar.app.log.parser.ApacheErrorLogParser;
import fr.devinsy.logar.app.log.parser.EmptyLogParser;
import fr.devinsy.logar.app.log.parser.LineParser;
import fr.devinsy.logar.app.log.parser.NginxErrorLogParser;
import fr.devinsy.logar.util.LineIterator;

/**
 * The Class LogFile.
 */
public final class LogFile
{
    private static Logger logger = LoggerFactory.getLogger(LogFile.class);

    /**
     * Instantiates a new log file.
     */
    private LogFile()
    {
    }

    /**
     * Gets the parser.
     *
     * @param file
     *            the file
     * @return the parser
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static LineParser getParser(final File file) throws IOException
    {
        LineParser result;

        if (file.getName().contains("access"))
        {
            result = new AccessLogParser();
        }
        else if (file.getName().contains("error"))
        {
            String line = readFirstLine(file);

            if (line == null)
            {
                result = new EmptyLogParser();
            }
            else if (line.startsWith("["))
            {
                result = new ApacheErrorLogParser();
            }
            else
            {
                result = new NginxErrorLogParser();
            }
        }
        else
        {
            throw new IllegalArgumentException("Bad named file (missing access or error).");
        }

        //
        return result;
    }

    /**
     * Gets the type.
     *
     * @param file
     *            the file
     * @return the type
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String getType(final File file) throws IOException
    {
        String result;

        LineParser parser = getParser(file);

        if (parser == null)
        {
            result = "*/*";
        }
        else
        {
            result = parser.getMime();
        }

        //
        return result;
    }

    /**
     * Parses the log file.
     *
     * @param file
     *            the file
     * @return the logs
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Logs parseLogFile(final File file) throws IOException
    {
        Logs result;

        result = parseLogFile(file, LogMode.NORMAL);

        //
        return result;
    }

    /**
     * Parses the log file.
     *
     * @param file
     *            the file
     * @param mode
     *            the mode
     * @return the logs
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Logs parseLogFile(final File file, final LogMode mode) throws IOException
    {
        Logs result;

        if (file == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else
        {
            LineParser lineParser = getParser(file);

            result = new Logs();
            LineIterator iterator = new LineIterator(file);
            while (iterator.hasNext())
            {
                String line = iterator.next();
                Log log = lineParser.parse(line);
                if (mode == LogMode.REDUCED)
                {
                    log.reduce();
                }
                result.add(log);
            }
        }

        //
        return result;
    }

    /**
     * Read first line.
     *
     * @param file
     *            the file
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private static String readFirstLine(final File file) throws IOException
    {
        String result;

        LineIterator iterator = null;
        try
        {
            iterator = new LineIterator(file);
            if (iterator.hasNext())
            {
                result = iterator.next();
            }
            else
            {
                result = null;
            }
        }
        finally
        {
            if (iterator != null)
            {
                iterator.close();
            }
        }

        //
        return result;
    }

    /**
     * Save logs.
     *
     * @param target
     *            the target
     * @param logs
     *            the logs
     * @throws FileNotFoundException
     *             the file not found exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void saveLogs(final File target, final Logs logs) throws FileNotFoundException, IOException
    {
        PrintWriter out = null;
        try
        {
            if (StringUtils.endsWithAny(target.getName(), ".gz", ".gz.tmp"))
            {
                out = new PrintWriter(new GZIPOutputStream(new FileOutputStream(target)));
            }
            else
            {
                out = new PrintWriter(new FileOutputStream(target));
            }

            for (Log log : logs)
            {
                out.println(log.getLine());
            }
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Sort log file.
     *
     * @param file
     *            the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void sortLogFile(final File file) throws IOException
    {
        File workFile = new File(file.getParent(), file.getName() + ".tmp");

        Logs logs = parseLogFile(file, LogMode.REDUCED);
        logs.sortByDatetime();
        saveLogs(workFile, logs);

        File backup = new File(file.getParentFile(), file.getName() + ".bak");
        if (file.renameTo(backup))
        {
            if (!workFile.renameTo(file))
            {
                backup.renameTo(file);
            }
        }

        // Check.
        try
        {
            String out = CmdExecUtils.run("/bin/bash -c \"zcat " + file.getAbsolutePath() + "| sort | sha1sum  \"");
            System.out.print(out);
            out = CmdExecUtils.run("/bin/bash -c \"zcat " + file.getAbsolutePath() + ".bak | sort | sha1sum  \"");
            System.out.println(out);
        }
        catch (CmdExecException exception)
        {
            exception.printStackTrace();
        }
    }
}
