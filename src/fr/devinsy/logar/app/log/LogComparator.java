/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.log;

import java.time.LocalDateTime;
import java.util.Comparator;

import fr.devinsy.logar.util.CompareUtils;

/**
 * The Class LogComparator.
 */
public class LogComparator implements Comparator<Log>
{
    public enum Sorting
    {
        DATETIME
    }

    private Sorting sorting;

    /**
     * Instantiates a new organization comparator.
     *
     * @param sorting
     *            the sorting
     */
    public LogComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @return the int
     */
    @Override
    public int compare(final Log alpha, final Log bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     * Compare.
     *
     * @param alpha
     *            the alpha
     * @param bravo
     *            the bravo
     * @param sorting
     *            the sorting
     * @return the int
     */
    public static int compare(final Log alpha, final Log bravo, final Sorting sorting)
    {
        int result;

        if (sorting == null)
        {
            result = 0;
        }
        else
        {
            switch (sorting)
            {
                default:
                case DATETIME:
                    result = CompareUtils.compare(getDatetime(alpha), getDatetime(bravo));
                break;
            }
        }

        //
        return result;
    }

    /**
     * Gets the datetime.
     *
     * @param source
     *            the source
     * @return the datetime
     */
    public static LocalDateTime getDatetime(final Log source)
    {
        LocalDateTime result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.getDatetime();
        }

        //
        return result;
    }
}
