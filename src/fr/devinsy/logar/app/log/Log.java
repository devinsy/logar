/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.log;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Log.
 */
public final class Log
{
    private static Logger logger = LoggerFactory.getLogger(Log.class);

    // Generic attributes.
    private String line;
    private LocalDateTime datetime;
    private String datetimeValue;

    // Specific access log attributes.
    private String ip;
    private String user;
    private String request;
    private String status;
    private String bodyByteSent;
    private String referer;
    private String userAgent;

    // Specific error log attributes.
    private String level;
    private String message;

    /**
     * Instantiates a new log.
     *
     * @param log
     *            the log
     */
    public Log(final Log log)
    {
        this.line = log.getLine();
        this.datetime = log.getDatetime();
        this.datetimeValue = log.getDatetimeValue();

        this.ip = log.getIp();
        this.user = log.getUser();
        this.request = log.getRequest();
        this.status = log.getStatus();
        this.bodyByteSent = log.getBodyByteSent();
        this.referer = log.getReferer();
        this.userAgent = log.getUserAgent();

        this.level = log.getLevel();
        this.message = log.getMessage();
    }

    /**
     * Instantiates a new log.
     *
     * @param line
     *            the line
     * @param datetime
     *            the datetime
     */
    public Log(final String line, final LocalDateTime datetime)
    {
        this.line = line;
        this.datetime = datetime;
        this.datetimeValue = null;

        this.ip = null;
        this.user = null;
        this.request = null;
        this.referer = null;
        this.userAgent = null;

        this.level = null;
        this.message = null;
    }

    /**
     * Concate access log.
     */
    public void concateAccessLog()
    {
        this.line = String.format("%s - %s [%s] \"%s\" %s %s \"%s\" \"%s\"",
                this.ip, this.user, this.datetimeValue, this.request, this.status, this.bodyByteSent, this.referer, this.userAgent);
    }

    /**
     * Concate error log.
     */
    public void concateErrorLog()
    {
        this.line = String.format("%s [%s] %s",
                this.datetimeValue, this.level, this.message);
    }

    /**
     * Gets the body byte sent.
     *
     * @return the body byte sent
     */
    public String getBodyByteSent()
    {
        return this.bodyByteSent;
    }

    /**
     * Gets the datetime.
     *
     * @return the datetime
     */
    public LocalDateTime getDatetime()
    {
        return this.datetime;
    }

    /**
     * Gets the datetime value.
     *
     * @return the datetime value
     */
    public String getDatetimeValue()
    {
        return this.datetimeValue;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp()
    {
        return this.ip;
    }

    /**
     * Gets the level.
     *
     * @return the level
     */
    public String getLevel()
    {
        return this.level;
    }

    /**
     * Gets the line.
     *
     * @return the line
     */
    public String getLine()
    {
        return this.line;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage()
    {
        return this.message;
    }

    /**
     * Gets the referer.
     *
     * @return the referer
     */
    public String getReferer()
    {
        return this.referer;
    }

    /**
     * Gets the request.
     *
     * @return the request
     */
    public String getRequest()
    {
        return this.request;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public String getStatus()
    {
        return this.status;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public String getUser()
    {
        return this.user;
    }

    /**
     * Gets the user agent.
     *
     * @return the user agent
     */
    public String getUserAgent()
    {
        return this.userAgent;
    }

    /**
     * Reduce.
     */
    public void reduce()
    {
        this.datetimeValue = null;
        this.request = null;
        this.referer = null;
        this.userAgent = null;
        this.status = null;
        this.bodyByteSent = null;

        this.level = null;
        this.message = null;
    }

    /**
     * Sets the body byte sent.
     *
     * @param bodyByteSent
     *            the new body byte sent
     */
    public void setBodyByteSent(final String bodyByteSent)
    {
        this.bodyByteSent = bodyByteSent;
    }

    /**
     * Sets the datetime value.
     *
     * @param datetimeValue
     *            the new datetime value
     */
    public void setDatetimeValue(final String datetimeValue)
    {
        this.datetimeValue = datetimeValue;
    }

    /**
     * Sets the ip.
     *
     * @param ip
     *            the new ip
     */
    public void setIp(final String ip)
    {
        this.ip = ip;
    }

    /**
     * Sets the level.
     *
     * @param level
     *            the new level
     */
    public void setLevel(final String level)
    {
        this.level = level;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(final String message)
    {
        this.message = message;
    }

    /**
     * Sets the referer.
     *
     * @param referer
     *            the new referer
     */
    public void setReferer(final String referer)
    {
        this.referer = referer;
    }

    /**
     * Sets the request.
     *
     * @param request
     *            the new request
     */
    public void setRequest(final String request)
    {
        this.request = request;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(final String status)
    {
        this.status = status;
    }

    /**
     * Sets the user.
     *
     * @param user
     *            the new user
     */
    public void setUser(final String user)
    {
        this.user = user;
    }

    /**
     * Sets the user agent.
     *
     * @param userAgent
     *            the new user agent
     */
    public void setUserAgent(final String userAgent)
    {
        this.userAgent = userAgent;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String result;

        result = this.line;

        //
        return result;
    }
}
