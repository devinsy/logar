/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.log;

import java.util.ArrayList;

/**
 * The Class Logs.
 */
public class Logs extends ArrayList<Log>
{
    private static final long serialVersionUID = 869606603972696142L;

    /**
     * Instantiates a new logs.
     */
    public Logs()
    {
        super();
    }

    /**
     * Instantiates a new logs.
     *
     * @param initialCapacity
     *            the initial capacity
     */
    public Logs(final int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * Sort.
     *
     * @param sorting
     *            the sorting
     * @return the logs
     */
    public Logs sort(final LogComparator.Sorting sorting)
    {
        Logs result;

        sort(new LogComparator(sorting));

        result = this;

        //
        return result;
    }

    /**
     * Sort by datetime.
     *
     * @return the logs
     */
    public Logs sortByDatetime()
    {
        Logs result;

        result = sort(LogComparator.Sorting.DATETIME);

        //
        return result;
    }
}
