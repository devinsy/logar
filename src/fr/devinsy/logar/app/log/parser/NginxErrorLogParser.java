/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.log.parser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.logar.app.log.Log;
import fr.devinsy.logar.app.log.LogType;

/**
 * The Class NginxErrorLogParser.
 */
public final class NginxErrorLogParser implements LineParser
{
    private static Logger logger = LoggerFactory.getLogger(NginxErrorLogParser.class);

    public static Pattern NGINX_ERRORLOG_LINE_PATTERN = Pattern.compile("^(?<datetime>\\S+\\s\\S+)\\s\\[(?<level>[^\\]]*)\\]\\s(?<message>.*)$");

    /**
     * Instantiates a new nginx error log parser.
     */
    public NginxErrorLogParser()
    {
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#type()
     */
    @Override
    public String getMime()
    {
        String result;

        result = "Error/Nginx";

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#getType()
     */
    @Override
    public LogType getType()
    {
        LogType result;

        result = LogType.ERROR;

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#parse(java.lang.String)
     */
    @Override
    public Log parse(final String line)
    {
        Log result;

        try
        {
            Matcher matcher = NGINX_ERRORLOG_LINE_PATTERN.matcher(line);
            if (matcher.matches())
            {
                String value = matcher.group("datetime");
                LocalDateTime date = LocalDateTime.parse(value, DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").withLocale(Locale.ENGLISH));

                result = new Log(line, date);
                result.setDatetimeValue(matcher.group("datetime"));
                result.setLevel(matcher.group("level"));
                result.setMessage(matcher.group("message"));
            }
            else
            {
                throw new IllegalArgumentException("Bad line format: " + line);
            }
        }
        catch (DateTimeParseException exception)
        {
            throw new IllegalArgumentException("Bad line format (date): " + line, exception);
        }

        //
        return result;
    }
}
