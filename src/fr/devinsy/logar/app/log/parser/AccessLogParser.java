/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.log.parser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.logar.app.log.Log;
import fr.devinsy.logar.app.log.LogType;

/**
 * The Class NginxAccessLogParser.
 */
public final class AccessLogParser implements LineParser
{
    private static Logger logger = LoggerFactory.getLogger(AccessLogParser.class);

    public static Pattern COMBINED_ACCESSLOG_LINE_PATTERN = Pattern.compile(
            "^(?<remoteAddress>[a-zA-F0-9\\\\:\\\\.]+) - (?<remoteUser>[^\\[]+) \\[(?<datetime>[^\\]]+)\\] \"(?<request>([^\"]|\\\")*)\" (?<status>\\d+) (?<bodyBytesSent>\\d+) \"(?<referer>([^\"]|\\\")*)\" \"(?<userAgent>([^\"]|\\\")*)\".*$");

    /**
     * Instantiates a new log parser.
     */
    public AccessLogParser()
    {
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#type()
     */
    @Override
    public String getMime()
    {
        String result;

        result = "Access/*";

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#getType()
     */
    @Override
    public LogType getType()
    {
        LogType result;

        result = LogType.ACCESS;

        //
        return result;
    }

    /**
     * From access log.
     *
     * @param line
     *            the line
     * @return the log
     * 
     *         log_format combined '$remote_addr - $remote_user [$time_local] '
     *         '"$request" $status $body_bytes_sent ' '"$http_referer"
     *         "$http_user_agent"';
     */
    @Override
    public Log parse(final String line)
    {
        Log result;

        try
        {
            Matcher matcher = COMBINED_ACCESSLOG_LINE_PATTERN.matcher(line);
            if (matcher.matches())
            {
                String dateTimeValue = matcher.group("datetime");
                LocalDateTime dateTime = LocalDateTime.parse(dateTimeValue, DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss Z").withLocale(Locale.ENGLISH));

                result = new Log(line, dateTime);

                result.setDatetimeValue(matcher.group("datetime"));
                result.setIp(matcher.group("remoteAddress"));
                result.setUser(matcher.group("remoteUser"));
                result.setRequest(matcher.group("request"));
                result.setStatus(matcher.group("status"));
                result.setBodyByteSent(matcher.group("bodyBytesSent"));
                result.setReferer(matcher.group("referer"));
                result.setUserAgent(matcher.group("userAgent"));
            }
            else
            {
                throw new IllegalArgumentException("Bad line format: " + line);
            }
        }
        catch (DateTimeParseException exception)
        {
            throw new IllegalArgumentException("Bad line format (date): " + line, exception);
        }

        //
        return result;
    }
}
