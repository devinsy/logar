/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.log.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.logar.app.log.Log;
import fr.devinsy.logar.app.log.LogType;

/**
 * The Class EmptyLogParser.
 */
public final class EmptyLogParser implements LineParser
{
    private static Logger logger = LoggerFactory.getLogger(EmptyLogParser.class);

    /**
     * Instantiates a new empty log parser.
     */
    public EmptyLogParser()
    {
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#type()
     */
    @Override
    public String getMime()
    {
        String result;

        result = "*/*";

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#getType()
     */
    @Override
    public LogType getType()
    {
        LogType result;

        result = LogType.WILDCARD;

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.logar.app.log.parser.LineParser#parse(java.lang.String)
     */
    @Override
    public Log parse(final String line)
    {
        Log result;

        result = null;

        //
        return result;
    }
}
