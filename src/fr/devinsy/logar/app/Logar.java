/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeParseException;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.logar.app.anonymizer.Anonymizer;
import fr.devinsy.logar.app.log.Log;
import fr.devinsy.logar.app.log.LogFile;
import fr.devinsy.logar.app.log.LogType;
import fr.devinsy.logar.app.log.parser.AccessLogParser;
import fr.devinsy.logar.app.log.parser.LineParser;
import fr.devinsy.logar.stats.UserAgentStator;
import fr.devinsy.logar.util.Chrono;
import fr.devinsy.logar.util.Files;
import fr.devinsy.logar.util.FilesUtils;
import fr.devinsy.logar.util.LineIterator;
import fr.devinsy.strings.StringList;

/**
 * The Class Logar.
 */
public final class Logar
{
    private static Logger logger = LoggerFactory.getLogger(Logar.class);

    public static String LOGFILE_PATTERN = "^.+(\\.|_)(log|log\\.gz|log\\.\\d+|log\\.\\d+\\.gz)$";

    /**
     * Instantiates a new log tool.
     */
    private Logar()
    {
    }

    /**
     * Anonymize.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void anonymize(final File source) throws IOException
    {
        anonymize(source, null);
    }

    /**
     * Anonymize.
     *
     * @param source
     *            the source
     * @param mapFile
     *            the map file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void anonymize(final File source, final File mapFile) throws IOException
    {
        if (source == null)
        {
            throw new IllegalArgumentException("Null source file.");
        }
        else if (!source.exists())
        {
            throw new IllegalArgumentException("Source file does not exist: " + source);
        }
        else
        {
            Anonymizer anonymizer = new Anonymizer();
            anonymizer.loadMapTable(mapFile);
            System.out.println("Table size=" + anonymizer.getMapTable().size());

            Files files = FilesUtils.search(source, LOGFILE_PATTERN).keepFileType().removeContaining("-anon.").sortByName();
            anonymize(files, mapFile);
        }
    }

    /**
     * Anonymize.
     *
     * @param files
     *            the files
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void anonymize(final Files files) throws IOException
    {
        anonymize(files, null);
    }

    /**
     * Anonymize.
     *
     * @param files
     *            the files
     * @param mapFile
     *            the map file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void anonymize(final Files files, final File mapFile) throws IOException
    {
        if (files == null)
        {
            throw new IllegalArgumentException("Null source file.");
        }
        else
        {
            Anonymizer anonymizer = new Anonymizer();
            anonymizer.loadMapTable(mapFile);
            System.out.println("Table size=" + anonymizer.getMapTable().size());

            logger.info("file count={}", files.size());
            for (File file : files)
            {
                anonymizer.anonymize(file);
            }

            System.out.println("Final table size: " + anonymizer.getMapTable().size());

            if (mapFile != null)
            {
                System.out.println("Table size=" + anonymizer.getMapTable().size());
                anonymizer.SaveMapTable(mapFile);
            }
        }
    }

    /**
     * Archive.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     * @param dry
     *            the dry
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void archive(final File source, final File target, final OnOffOption dry) throws IOException
    {
        archiveAccessFiles(source, target, dry);
        archiveErrorFiles(source, target, dry);
    }

    /**
     * Archive access files.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     * @param dry
     *            the dry
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void archiveAccessFiles(final File source, final File target, final OnOffOption dry) throws IOException
    {
        if (source == null)
        {
            throw new IllegalArgumentException("Null parameter [source]");
        }
        else if (!source.isDirectory())
        {
            throw new IllegalArgumentException("Source parameter is not a directory.");
        }
        else if (!target.isDirectory())
        {
            throw new IllegalArgumentException("Target parameter is not a directory.");
        }
        else
        {
            YearMonth targetYearMonth = YearMonth.now().minusMonths(1);
            Stats counter = new Stats();

            LineParser lineParser = new AccessLogParser();
            for (File directory : Files.of(source).removeHidden().keepDirectoryType().sortByName())
            {
                String targetFileName = String.format("%s-access-%s.log.gz", directory.getName(), targetYearMonth.toString());
                File targetDirectory = new File(target, directory.getName());
                File targetFile = new File(targetDirectory, targetFileName);
                logger.info("== {} -> {}", directory.getName(), targetFile.getAbsoluteFile());

                PrintWriter out = null;
                if (dry.isOff())
                {
                    targetDirectory.mkdirs();
                    out = new PrintWriter(new GZIPOutputStream(new FileOutputStream(targetFile)));
                }

                for (File file : Files.of(directory).sortByName())
                {
                    if ((!file.isDirectory()) && (file.getName().contains("access")))
                    {
                        logger.info(file.getName());
                        try
                        {
                            LineIterator iterator = new LineIterator(file);
                            while (iterator.hasNext())
                            {
                                String line = iterator.next();
                                counter.incLineCount();

                                try
                                {
                                    Log log = lineParser.parse(line);
                                    counter.incSuccessLineCount();

                                    if (YearMonth.from(log.getDatetime()).equals(targetYearMonth))
                                    {
                                        if (dry.isOff())
                                        {
                                            out.println(line);
                                        }
                                    }
                                }
                                catch (IllegalArgumentException exception)
                                {
                                    System.err.println("Bad line format [" + line + "]");
                                    counter.incErrorLineCount();
                                }
                            }

                            counter.incSuccessFileCount();
                        }
                        catch (IOException exception)
                        {
                            System.err.println("Error with file [" + file.getAbsolutePath() + "]");
                            exception.printStackTrace();
                            counter.incErrorFileCount();
                        }
                        finally
                        {
                            counter.incFileCount();
                        }
                    }
                }

                IOUtils.closeQuietly(out);
            }

            System.out.println("=====================================================");
            counter.stop();
            System.out.println(counter.toString());
        }
    }

    /**
     * Archive error files.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     * @param dry
     *            the dry
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void archiveErrorFiles(final File source, final File target, final OnOffOption dry) throws IOException
    {
        if (source == null)
        {
            throw new IllegalArgumentException("Null parameter [source]");
        }
        else if (!source.isDirectory())
        {
            throw new IllegalArgumentException("Source parameter is not a directory.");
        }
        else if (!target.isDirectory())
        {
            throw new IllegalArgumentException("Target parameter is not a directory.");
        }
        else
        {
            YearMonth targetYearMonth = YearMonth.now().minusMonths(1);
            Stats counter = new Stats();
            counter.start();

            for (File directory : Files.of(source).removeHidden().keepDirectoryType().sortByName())
            {
                String targetFileName = String.format("%s-error-%s.log.gz", directory.getName(), targetYearMonth.toString());
                File targetDirectory = new File(target, directory.getName());
                File targetFile = new File(targetDirectory, targetFileName);
                logger.info("== {} -> {}", directory.getName(), targetFile.getAbsoluteFile());
                PrintWriter out = null;
                if (dry.isOff())
                {
                    targetDirectory.mkdirs();
                    out = new PrintWriter(new GZIPOutputStream(new FileOutputStream(targetFile)));
                }

                for (File file : Files.of(directory).sortByName())
                {
                    if ((!file.isDirectory()) && (file.getName().contains("error")))
                    {
                        LineParser lineParser = LogFile.getParser(file);
                        // logger.info(file.getName());
                        try
                        {
                            LineIterator iterator = new LineIterator(file);
                            while (iterator.hasNext())
                            {
                                String line = iterator.next();
                                counter.incLineCount();

                                try
                                {
                                    Log log = lineParser.parse(line);
                                    counter.incSuccessLineCount();

                                    if (YearMonth.from(log.getDatetime()).equals(targetYearMonth))
                                    {
                                        if (dry.isOff())
                                        {
                                            out.println(line);
                                        }
                                    }
                                }
                                catch (IllegalArgumentException exception)
                                {
                                    System.err.println("Bad line format [" + line + "]");
                                    counter.incErrorLineCount();
                                }
                            }
                            counter.incSuccessFileCount();
                        }
                        catch (IOException exception)
                        {
                            System.err.println("Error with file [" + file.getAbsolutePath() + "]");
                            exception.printStackTrace();
                            counter.incErrorFileCount();
                        }
                        finally
                        {
                            counter.incFileCount();
                        }
                    }
                }

                IOUtils.closeQuietly(out);
            }

            System.out.println("=====================================================");
            counter.stop();
            System.out.println(counter.toString());
        }
    }

    /**
     * Check log file.
     *
     * @param file
     *            the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void checkLogFile(final File file) throws IOException
    {
        if (file == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else if (!file.isFile())
        {
            throw new IllegalArgumentException("Parameter is not a file [" + file + "]");
        }
        else
        {
            System.out.println("== Check parse log for [" + file.getName() + "]");
            LineParser parser = LogFile.getParser(file);

            int lineCount = 0;
            int badLineCount = 0;
            try
            {
                LineIterator iterator = new LineIterator(file);
                while (iterator.hasNext())
                {
                    String line = iterator.next();
                    lineCount += 1;

                    try
                    {
                        parser.parse(line).getDatetime();
                    }
                    catch (IllegalArgumentException exception)
                    {
                        System.out.println("Bad format line: " + line);
                        badLineCount += 1;
                        exception.printStackTrace();
                    }
                    catch (DateTimeParseException exception)
                    {
                        System.out.println("Bad datetime format: " + line);
                        badLineCount += 1;
                    }
                }
            }
            catch (IOException exception)
            {
                System.err.println("Error with file [" + file.getAbsolutePath() + "]");
                exception.printStackTrace();
            }

            if (badLineCount > 0)
            {
                System.out.println("Bad line count: " + badLineCount + "/" + lineCount);
            }
        }
    }

    /**
     * Check log files.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void checkLogFiles(final File source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else if (!source.exists())
        {
            System.out.println("Missing source to check [" + source + "]");
        }
        else
        {
            checkLogFiles(new Files(source));
        }
    }

    /**
     * Check log files.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void checkLogFiles(final Files source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else
        {
            for (File file : source)
            {
                checkLogFile(file);
            }
        }
    }

    /**
     * Check sort.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void checkSort(final File source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else if (!source.exists())
        {
            System.out.println("Missing source to check [" + source + "]");
        }
        else
        {
            Files files = FilesUtils.search(source, LOGFILE_PATTERN).sortByName();

            for (File file : files)
            {
                checkSortFile(file);
            }
        }
    }

    /**
     * Check sort.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void checkSort(final Files source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else
        {
            for (File file : source)
            {
                checkSortFile(file);
            }
        }
    }

    /**
     * Check sort file.
     *
     * @param file
     *            the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void checkSortFile(final File file) throws IOException
    {
        if (file == null)
        {
            throw new IllegalArgumentException("Null parameter [source]");
        }
        else if (!file.isFile())
        {
            throw new IllegalArgumentException("Source parameter is not a file.");
        }
        else
        {
            System.out.println("== Check sort for [" + file.getName() + "]");
            LineParser lineParser = LogFile.getParser(file);
            LocalDateTime currentDate = null;
            int lineCount = 0;
            int badLineCount = 0;
            LineIterator iterator = new LineIterator(file);
            while (iterator.hasNext())
            {
                String line = iterator.next();
                lineCount += 1;
                LocalDateTime date;
                date = lineParser.parse(line).getDatetime();

                if ((currentDate != null) && (date.isBefore(currentDate)))
                {
                    System.out.println(String.format("break detected: %d %s", lineCount, currentDate.toString()));
                    badLineCount += 1;
                }
                currentDate = date;
            }

            if (badLineCount > 0)
            {
                System.out.println("Bad line count: " + badLineCount + "/" + lineCount);
            }
        }
    }

    /**
     * Extract user agent.
     *
     * @param source
     *            the source
     * @param options
     *            the options
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void extract(final File source, final ExtractOptions options) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else if (options == null)
        {
            System.out.println("Undefined option.");
        }
        else if (!source.exists())
        {
            System.out.println("Missing source to sort.");
        }
        else
        {
            Files files = FilesUtils.search(source, LOGFILE_PATTERN).removeHidden().keep(".*access.*").sortByName();
            // System.out.println(files.size());
            System.err.println(files.size());
            for (File file : files)
            {
                System.err.println("== Extract userAgent for [" + file.getName() + "]");
                try
                {
                    LineParser lineParser = LogFile.getParser(file);
                    LineIterator iterator = new LineIterator(file);
                    while (iterator.hasNext())
                    {
                        String line = iterator.next();
                        // System.out.println(line);
                        try
                        {
                            Log log = lineParser.parse(line);

                            StringList extract = new StringList();
                            if (options.getIp().isOn())
                            {
                                extract.append(log.getIp());
                            }

                            if (options.getUser().isOn())
                            {
                                extract.append(log.getUser());
                            }

                            if (options.getDatetime().isOn())
                            {
                                extract.append(log.getDatetime());
                            }

                            if (options.getUserAgent().isOn())
                            {
                                extract.append(log.getUserAgent());
                            }

                            System.out.println(extract.toStringSeparatedBy(" "));
                        }
                        catch (IllegalArgumentException exception)
                        {
                            System.out.println("Bad format line: " + line);
                            exception.printStackTrace();
                        }
                        catch (DateTimeParseException exception)
                        {
                            System.out.println("Bad datetime format: " + line);
                        }
                    }
                }
                catch (IOException exception)
                {
                    System.err.println("Error with file [" + file.getAbsolutePath() + "]");
                    exception.printStackTrace();
                }
            }
        }
    }

    /**
     * Identify.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void identify(final File source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else if (!source.exists())
        {
            System.out.println("Missing source to sort [" + source + "]");
        }
        else
        {
            identify(new Files(source));
        }
    }

    /**
     * Identify.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void identify(final Files source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else
        {
            for (File file : source)
            {
                String type = LogFile.getType(file);

                System.out.println(StringUtils.rightPad(type, 20) + file.getName());
            }
        }
    }

    /**
     * Sort.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void sort(final File source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else if (!source.exists())
        {
            System.out.println("Missing source to sort [" + source + "]");
        }
        else
        {
            sort(new Files(source));
        }
    }

    /**
     * Sort.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void sort(final Files source) throws IOException
    {
        if (source == null)
        {
            System.out.println("Undefined source.");
        }
        else
        {
            for (File file : source)
            {
                System.out.println("== Sort for [" + file.getName() + "]");
                LogFile.sortLogFile(file);
            }
        }
    }

    /**
     * Stat user agents.
     *
     * @param source
     *            the source
     */
    public static void statUserAgents(final File source)
    {
        statUserAgents(new Files(source));
    }

    /**
     * Stat user agents.
     *
     * @param source
     *            the source
     */
    public static void statUserAgents(final Files source)
    {
        for (File file : source)
        {
            statUserAgentsForFile(file);
        }
    }

    /**
     * Stat user agents for file.
     *
     * @param file
     *            the file
     */
    public static void statUserAgentsForFile(final File file)
    {
        if (file == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else if (!file.isFile())
        {
            throw new IllegalArgumentException("Parameter is not a file.");
        }
        else
        {
            System.out.println("== Stat UserAgents for [" + file.getName() + "]");

            int lineCount = 0;
            int badLineCount = 0;
            try
            {
                LineParser lineParser = LogFile.getParser(file);
                UserAgentStator stator = new UserAgentStator();
                LineIterator iterator = new LineIterator(file);
                Chrono chrono = new Chrono().start();
                long lastDuration = 0;
                while (iterator.hasNext())
                {
                    String line = iterator.next();
                    lineCount += 1;

                    if ((chrono.duration() % 60 == 0) && (chrono.duration() != lastDuration))
                    {
                        lastDuration = chrono.duration();
                        System.out.println(chrono.format() + " line counter " + lineCount);
                    }

                    try
                    {
                        Log log = lineParser.parse(line);
                        stator.putLog(log);
                    }
                    catch (IllegalArgumentException exception)
                    {
                        System.out.println("Bad format line: " + line);
                        badLineCount += 1;
                        exception.printStackTrace();
                    }
                    catch (DateTimeParseException exception)
                    {
                        System.out.println("Bad datetime format: " + line);
                        badLineCount += 1;
                    }
                }

                //
                System.out.println("Log count         =" + stator.getLogCount());
                System.out.println("Ip count          =" + stator.getIps().size());
                System.out.println("UserAgent count   =" + stator.getUserAgents().size());
                System.out.println("IpUserAgent count =" + stator.getIpUserAgents().size());

                System.out.println("out=" + file.getCanonicalFile().getParentFile());

                stator.saveIpList(new File(file.getParentFile(), "stator-ip.list"));
                stator.saveUserAgentList(new File(file.getParentFile(), "stator-ua.list"));
                stator.saveIpUserAgentList(new File(file.getParentFile(), "stator-ipua.list"));

                stator.shrink();
                System.out.println("UserAgent count   =" + stator.getUserAgents().size());
                System.out.println("IpUserAgent count =" + stator.getIpUserAgents().size());
                stator.saveUserAgentList(new File(file.getParentFile(), "stator-ua-short.list"));
                stator.saveIpUserAgentList(new File(file.getParentFile(), "stator-ipua-short.list"));

                stator.computeIpLinkCountForUserAgent();
                stator.saveUserAgentLinkCount(new File(file.getParentFile(), "stator-stats.list"));
            }
            catch (IOException exception)
            {
                System.err.println("Error with file [" + file.getAbsolutePath() + "]");
                exception.printStackTrace();
            }

            if (badLineCount > 0)
            {
                System.out.println("Bad line count: " + badLineCount + "/" + lineCount);
            }
        }
    }

    /**
     * Check concate.
     *
     * @param source
     *            the source
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void testConcate(final File source) throws IOException
    {
        Files files = FilesUtils.searchEndingWith(source, ".log", ".log.gz").keepFileType().removeContaining("-anon.log").sortByName();

        for (File file : files)
        {
            testConcateFile(file);
        }
    }

    /**
     * Test concate file.
     *
     * @param file
     *            the file
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void testConcateFile(final File file) throws IOException
    {
        if (file == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else if (!file.isFile())
        {
            throw new IllegalArgumentException("Parameter is not a file.");
        }
        else
        {
            System.out.println("== Test concate log for [" + file.getName() + "]");
            LineParser lineParser = LogFile.getParser(file);

            int lineCount = 0;
            int badLineCount = 0;
            try
            {
                LineIterator iterator = new LineIterator(file);
                while (iterator.hasNext())
                {
                    String line = iterator.next();
                    lineCount += 1;

                    try
                    {
                        Log source = lineParser.parse(line);
                        Log target = new Log(source);
                        if (lineParser.getType() == LogType.ACCESS)
                        {
                            target.concateAccessLog();
                        }
                        else if (lineParser.getType() == LogType.ERROR)
                        {
                            target.concateErrorLog();
                        }

                        if (!StringUtils.equals(source.getLine(), target.getLine()))
                        {
                            System.out.println("Bad concat (1): " + source);
                            System.out.println("Bad concat (2): " + target);
                            badLineCount += 1;
                        }

                    }
                    catch (IllegalArgumentException exception)
                    {
                        System.out.println("Bad format line: " + line);
                        badLineCount += 1;
                        exception.printStackTrace();
                    }
                    catch (DateTimeParseException exception)
                    {
                        System.out.println("Bad datetime format: " + line);
                        badLineCount += 1;
                    }
                }
            }
            catch (IOException exception)
            {
                System.err.println("Error with file [" + file.getAbsolutePath() + "]");
                exception.printStackTrace();
            }

            if (badLineCount > 0)
            {
                System.out.println("Bad line count: " + badLineCount + "/" + lineCount);
            }
        }
    }
}
