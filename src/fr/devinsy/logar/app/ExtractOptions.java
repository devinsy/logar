/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app;

import org.apache.commons.lang3.StringUtils;

/**
 * The Enum DryOption.
 */
public class ExtractOptions
{
    OnOffOption ip;
    OnOffOption user;
    OnOffOption datetime;
    OnOffOption userAgent;

    /**
     * Instantiates a new extract options.
     */
    public ExtractOptions()
    {
        this.ip = OnOffOption.OFF;
        this.user = OnOffOption.OFF;
        this.datetime = OnOffOption.OFF;
        this.userAgent = OnOffOption.OFF;
    }

    /**
     * Gets the datetime.
     *
     * @return the datetime
     */
    public OnOffOption getDatetime()
    {
        return this.datetime;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public OnOffOption getIp()
    {
        return this.ip;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public OnOffOption getUser()
    {
        return this.user;
    }

    /**
     * Gets the user agent.
     *
     * @return the user agent
     */
    public OnOffOption getUserAgent()
    {
        return this.userAgent;
    }

    /**
     * Sets the datetime.
     *
     * @param datetime
     *            the new datetime
     */
    public void setDatetime(final OnOffOption datetime)
    {
        this.datetime = datetime;
    }

    /**
     * Sets the ip.
     *
     * @param ip
     *            the new ip
     */
    public void setIp(final OnOffOption ip)
    {
        this.ip = ip;
    }

    /**
     * Sets the user.
     *
     * @param user
     *            the new user
     */
    public void setUser(final OnOffOption user)
    {
        this.user = user;
    }

    /**
     * Sets the user agent.
     *
     * @param userAgent
     *            the new user agent
     */
    public void setUserAgent(final OnOffOption userAgent)
    {
        this.userAgent = userAgent;
    }

    /**
     * Of.
     *
     * @param source
     *            the source
     * @return the extract option
     */
    public static ExtractOptions of(final String source)
    {
        ExtractOptions result;

        result = new ExtractOptions();

        if (StringUtils.contains(source, "ip"))
        {
            result.setIp(OnOffOption.ON);
        }

        if (StringUtils.contains(source, "remoteuser"))
        {
            result.setUser(OnOffOption.ON);
        }

        if (StringUtils.contains(source, "datetime"))
        {
            result.setDatetime(OnOffOption.ON);
        }

        if (StringUtils.contains(source, "useragent"))
        {
            result.setUserAgent(OnOffOption.ON);
        }

        //
        return result;
    }
}
