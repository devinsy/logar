/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.anonymizer;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserGenerator.
 */
public final class UserGenerator
{
    private static Logger logger = LoggerFactory.getLogger(UserGenerator.class);

    /**
     * Instantiates a new user generator.
     */
    private UserGenerator()
    {
    }

    /**
     * Random.
     *
     * @param length
     *            the length
     * @return the string
     */
    public static String random(final int length)
    {
        String result;

        if (length < 0)
        {
            throw new IllegalArgumentException("Bad parameter: " + length);
        }
        else
        {
            result = RandomStringUtils.random(length);
        }

        //
        return result;
    }

    /**
     * Random.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String random(final String source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else if (source.equals("-"))
        {
            result = source;
        }
        else
        {
            result = RandomStringUtils.random(source.length(), "abcdefghijklmnopqrstuvwxyz");
        }

        //
        return result;
    }
}
