/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.anonymizer;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Ipv6Generator.
 */
public final class Ipv6Generator
{
    private static Logger logger = LoggerFactory.getLogger(Ipv6Generator.class);

    /**
     * Instantiates a new ipv 6 generator.
     */
    private Ipv6Generator()
    {
    }

    /**
     * Random.
     *
     * @param ip
     *            the source
     * @return the string
     */
    public static String random(final String ip)
    {
        String result;

        if (ip == null)
        {
            result = null;
        }
        else
        {
            StringBuffer buffer = new StringBuffer(ip.length());
            for (int index = 0; index < ip.length(); index++)
            {
                char c = ip.charAt(index);
                if (c == ':')
                {
                    buffer.append(c);
                }
                else if (index == 0)
                {
                    buffer.append('b');
                }
                else if (index == 1)
                {
                    buffer.append('a');
                }
                else if (index == 2)
                {
                    buffer.append('d');
                }
                else
                {
                    buffer.append(RandomStringUtils.random(1, "0123456789abcdef"));
                }
            }
            result = buffer.toString();

            while (StringUtils.equals(result, ip))
            {
                result = random(ip);
            }
        }

        //
        return result;
    }
}
