/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.anonymizer;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserAgentGenerator.
 */
public final class UserAgentGenerator
{
    private static Logger logger = LoggerFactory.getLogger(UserAgentGenerator.class);

    /**
     * Instantiates a new user agent generator.
     */
    private UserAgentGenerator()
    {
    }

    /**
     * Shrink.
     *
     * @param source
     *            the source
     * @return the string
     */
    public static String anonymize(final String source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else if (source.equals("-"))
        {
            result = source;
        }
        else
        {
            // StringBuffer buffer = new StringBuffer(source.length());
            // String targets = "./[]_-0123456789";
            // for (int index = 0; index < source.length(); index++)
            // {
            // char letter = source.charAt(index);
            // if (!StringUtils.containsAny(targets, letter))
            // {
            // buffer.append(letter);
            // }
            // }
            //
            result = source;
            result = RegExUtils.replaceAll(result, "\\[.+\\]", "");
            result = RegExUtils.replaceAll(result, "https?://[^\\s\\)\\],;]+", "");
            result = RegExUtils.replaceAll(result, "\\(\\)", "");
            result = RegExUtils.replaceAll(result, "(B|b)uild ?[~0-9a-zA-Z/\\.-_@]+", "build");
            result = RegExUtils.replaceAll(result, "\\d{4}/\\d{2}/\\d{2}", "");
            result = RegExUtils.replaceAll(result, "(/|:|-|@)?[0-9][~0-9a-zA-Z\\.-_]+", "");
            // result = StringUtils.replaceChars(result, ".[]_-0123456789", "");

            result = StringUtils.rightPad(result, source.length());
        }

        //
        return result;
    }
}
