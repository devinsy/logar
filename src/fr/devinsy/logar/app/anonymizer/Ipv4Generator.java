/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.anonymizer;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Ipv4Generator.
 */
public final class Ipv4Generator
{
    private static Logger logger = LoggerFactory.getLogger(Ipv4Generator.class);

    /**
     * Instantiates a new ipv 4 generator.
     */
    private Ipv4Generator()
    {
    }

    /**
     * Gets the ipv 4 max length part.
     *
     * @param length
     *            the length (1..12)
     * @param column
     *            the column (4...1)
     * @return the ipv 4 max length part
     */
    public static int getIpv4MaxLengthPart(final int length, final int column)
    {
        int result;

        if ((length < 1) || (length > 12))
        {
            throw new IllegalArgumentException("Bad length value: " + length);
        }
        else if ((column < 1) || (column > 12))
        {
            throw new IllegalArgumentException("Bad column value:" + column);
        }
        else
        {
            final int[] col4 = { 0, 0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3 };
            final int[] col3 = { 0, 0, 1, 2, 3, 3, 3, 3, 3, 0, 0, 0 };
            final int[] col2 = { 0, 1, 2, 3, 3, 3, 0, 0, 0, 0, 0, 0 };
            final int[] col1 = { 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            final int[][] table = { col1, col2, col3, col4 };

            result = table[column - 1][length - 1];

            if (result == 0)
            {
                throw new IllegalArgumentException(String.format("Zero detected (%d, %d).", length, column));
            }
        }

        //
        return result;
    }

    /**
     * Gets the ipv 4 max value part.
     *
     * @param length
     *            the length
     * @param column
     *            the column
     * @return the ipv 4 max value part
     */
    public static int getIpv4MaxValuePart(final int length, final int column)
    {
        int result;

        int max = getIpv4MaxLengthPart(length, column);

        switch (max)
        {
            case 1:
                result = 9;
            break;

            case 2:
                result = 99;
            break;

            case 3:
                result = 255;
            break;

            default:
                throw new IllegalArgumentException("Bad value: " + max);
        }

        //
        return result;
    }

    /**
     * Gets the ipv 4 min length part.
     *
     * @param length
     *            the length (1..12)
     * @param column
     *            the column (4...1)
     * @return the ipv 4 min length part
     */
    public static int getIpv4MinLengthPart(final int length, final int column)
    {
        int result;

        if ((length < 1) || (length > 12))
        {
            throw new IllegalArgumentException("Bad length value: " + length);
        }
        else if ((column < 1) || (column > 12))
        {
            throw new IllegalArgumentException("Bad column value:" + column);
        }
        else
        {
            final int[] col4 = { 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 3 };
            final int[] col3 = { 0, 0, 1, 1, 1, 1, 1, 2, 3, 0, 0, 0 };
            final int[] col2 = { 0, 1, 1, 1, 2, 3, 0, 0, 0, 0, 0, 0 };
            final int[] col1 = { 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            final int[][] table = { col1, col2, col3, col4 };

            result = table[column - 1][length - 1];

            if (result == 0)
            {
                throw new IllegalArgumentException(String.format("Zero detected (%d, %d).", length, column));
            }
        }

        //
        return result;
    }

    /**
     * Gets the ipv 4 min value part.
     *
     * @param length
     *            the length
     * @param column
     *            the column
     * @return the ipv 4 min value part
     */
    public static int getIpv4MinValuePart(final int length, final int column)
    {
        int result;

        int max = getIpv4MinLengthPart(length, column);

        switch (max)
        {
            case 1:
                result = 0;
            break;

            case 2:
                result = 10;
            break;

            case 3:
                result = 100;
            break;

            default:
                throw new IllegalArgumentException("Bad value: " + max);
        }

        //
        return result;
    }

    /**
     * Generates a ipv4 of a fixed length.
     *
     * @param length
     *            the length
     * @return the string
     */
    public static String random(final int length)
    {
        String result;

        if ((length < 7) || (length > 15))
        {
            throw new IllegalArgumentException("Bad parameter: " + length);
        }
        else
        {
            int size = length - 3;

            int a = (int) RandomUtils.nextLong(getIpv4MinValuePart(size, 4), getIpv4MaxValuePart(size, 4) + 1);
            size -= String.valueOf(a).length();

            int b = (int) RandomUtils.nextLong(getIpv4MinValuePart(size, 3), getIpv4MaxValuePart(size, 3) + 1);
            size -= String.valueOf(b).length();

            int c = (int) RandomUtils.nextLong(getIpv4MinValuePart(size, 2), getIpv4MaxValuePart(size, 2) + 1);
            size -= String.valueOf(c).length();

            int d = (int) RandomUtils.nextLong(getIpv4MinValuePart(size, 1), getIpv4MaxValuePart(size, 1) + 1);
            size -= String.valueOf(d).length();

            result = String.format("%d.%d.%d.%d", a, b, c, d);
        }

        //
        return result;
    }

    /**
     * Generates a ipv4 with the same length than the parameter.
     *
     * @param ip
     *            the source
     * @return the string
     */
    public static String random(final String ip)
    {
        String result;

        if (ip == null)
        {
            result = null;
        }
        else if (StringUtils.equalsAny(ip, "0.0.0.0", "127.0.0.1"))
        {
            result = ip;
        }
        else
        {
            result = random(ip.length());

            while (StringUtils.equals(result, ip))
            {
                result = random(ip);
            }
        }

        //
        return result;
    }
}
