/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.app.anonymizer;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.strings.StringSet;

/**
 * The Class AnonMap.
 */
public final class AnonMap
{
    private static Logger logger = LoggerFactory.getLogger(AnonMap.class);

    private HashMap<String, String> map;
    private HashMap<String, String> unmap;

    /**
     * Instantiates a new anon map.
     */
    public AnonMap()
    {
        this.map = new HashMap<String, String>();
        this.unmap = new HashMap<String, String>();
    }

    /**
     * Adds the all.
     *
     * @param source
     *            the source
     */
    public void addAll(final AnonMap source)
    {
        for (String key : source.getKeys())
        {
            String value = source.get(key);

            this.map.put(key, value);
            this.unmap.put(value, key);
        }
    }

    /**
     * Gets the ip.
     *
     * @param ip
     *            the ip
     * @return the ip
     */
    public String anonymizeIp(final String ip)
    {
        String result;

        if (ip == null)
        {
            result = null;
        }
        else
        {
            result = this.map.get(ip);

            if (result == null)
            {
                boolean ended = false;
                while (!ended)
                {
                    if (ip.contains(":"))
                    {
                        result = Ipv6Generator.random(ip);
                    }
                    else
                    {
                        result = Ipv4Generator.random(ip);
                    }

                    // Check it does not already exist.
                    if ((this.unmap.get(result) == null) && (this.map.get(result) == null))
                    {
                        this.map.put(ip, result);
                        this.unmap.put(result, ip);
                        ended = true;
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the user.
     *
     * @param user
     *            the user
     * @return the user
     */
    public String anonymizeUser(final String user)
    {
        String result;

        if (user == null)
        {
            result = null;
        }
        else if (user.equals("-"))
        {
            result = user;
        }
        else
        {
            result = this.map.get(user);

            if (result == null)
            {
                boolean ended = false;
                while (!ended)
                {
                    result = UserGenerator.random(user);

                    // Check it does not already exist.
                    if ((this.map.get(result) == null) && (this.unmap.get(result) == null))
                    {
                        this.map.put(user, result);
                        this.unmap.put(user, result);
                        ended = true;
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the.
     *
     * @param key
     *            the key
     * @return the string
     */
    public String get(final String key)
    {
        String result;

        if (key == null)
        {
            result = null;
        }
        else
        {
            result = this.map.get(key);
        }

        //
        return result;
    }

    /**
     * Gets the keys.
     *
     * @return the keys
     */
    public StringSet getKeys()
    {
        StringSet result;

        result = new StringSet(this.map.keySet());

        //
        return result;
    }

    /**
     * Put.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */
    public void put(final String key, final String value)
    {
        this.map.put(key, value);
    }

    /**
     * Size.
     *
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.map.size();

        //
        return result;
    }
}
