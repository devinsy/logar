/*
 * Copyright (C) 2021 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.cli;

import fr.devinsy.logar.app.LogarException;

/**
 * The Class LogarCLIException.
 */
public class LogarCLIException extends LogarException
{
    private static final long serialVersionUID = -907778172732888042L;

    /**
     * Instantiates a new log tool CLI exception.
     */
    public LogarCLIException()
    {
        super();
    }

    /**
     * Instantiates a new logar CLI exception.
     *
     * @param message
     *            the message
     */
    public LogarCLIException(final String message)
    {
        super(message);
    }

    /**
     * Instantiates a new logar CLI exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public LogarCLIException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instantiates a new logar CLI exception.
     *
     * @param cause
     *            the cause
     */
    public LogarCLIException(final Throwable cause)
    {
        super(cause);
    }
}