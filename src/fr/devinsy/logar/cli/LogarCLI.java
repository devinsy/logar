/*
 * Copyright (C) 2021-2024 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of Logar, simple tool to manage http log files.
 * 
 * Logar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Logar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Logar.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.logar.cli;

import java.io.File;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.logar.app.ExtractOptions;
import fr.devinsy.logar.app.Logar;
import fr.devinsy.logar.app.OnOffOption;
import fr.devinsy.logar.util.BuildInformation;
import fr.devinsy.logar.util.Files;
import fr.devinsy.logar.util.FilesUtils;
import fr.devinsy.strings.StringList;

/**
 * The Class <code>Logar</code> manages a Command Line Interface for Logar.
 * 
 */
public final class LogarCLI
{
    private static Logger logger = LoggerFactory.getLogger(LogarCLI.class);

    /**
     * Instantiates a new log tool CLI.
     */
    private LogarCLI()
    {
    }

    /**
     * Display help.
     */
    public static void displayHelp()
    {
        StringList message = new StringList();

        message.append("Logar CLI version ").appendln(BuildInformation.instance().version());
        message.appendln("Usage:");
        message.appendln("    logar [ -h | -help | --help ]");
        message.appendln("    logar [ -v | -version | --version ]");
        message.appendln("    logar anonymize        [-map <mapfile>] <fileordirectory>  anonymize ip and user");
        message.appendln("    logar archive [-dry]   <source> <target>                   archive previous month from /var/log/nginx/ tree");
        message.appendln("    logar check            <fileordirectory>                   check line format in log files");
        message.appendln("    logar checksort        <fileordirectory>                   check sort in log files");
        message.appendln("    logar extract <fields> <fileordirectory>                   extract one or more fields (ip,user,datetime,useragent) from log files");
        message.appendln("    logar identify         <fileordirectory>                   display type of log files");
        message.appendln("    logar sort             <fileordirectory>                   sort log files by datetime");
        message.appendln("    logar testconcate      <fileordirectory>                   test line concate in log files");

        System.out.println(message.toString());
    }

    /**
     * Display version.
     */
    public static void displayVersion()
    {
        StringList message = new StringList();

        message.appendln(BuildInformation.instance().version());

        System.out.println(message.toString());
    }

    /**
     * 
     * This method launch CLI.
     * 
     * @param args
     *            necessary arguments
     */
    public static void run(final String[] args)
    {
        // Set default catch.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\n";
                }
                else
                {
                    message = String.format("An error occured: %1s(%2s)", exception.getClass(), exception.getMessage());
                }

                logger.error("uncaughtException ", exception);
                logger.error(message);
                logger.info("Oups, an unexpected error occured. Please try again.");
            }
        });

        try
        {
            logger.info("{} Logar call: {}", LocalDateTime.now(), new StringList(args).toStringSeparatedBy(" "));

            if (CLIUtils.isMatching(args))
            {
                logger.info("No parameter.");
                displayHelp();
            }
            else if (CLIUtils.isMatching(args, "(-h|--h|--help)"))
            {
                displayHelp();
            }
            else if (CLIUtils.isMatching(args, "(-v|-version|--version)"))
            {
                displayVersion();
            }
            else if (CLIUtils.isMatchingEllipsis(args, "anonymize", "-map", ".+", ".+"))
            {
                File map = new File(args[2]);
                Files source = new Files();
                for (int index = 3; index < args.length; index++)
                {
                    File file = new File(args[index]);
                    Files filteredFiles = FilesUtils.search(file, Logar.LOGFILE_PATTERN).keepFileType().removeContaining("-anon.").sortByName();
                    source.addAll(filteredFiles);
                }
                Logar.anonymize(source, map);
            }
            else if (CLIUtils.isMatchingEllipsis(args, "anonymize", ".+"))
            {
                Files source = new Files();
                for (int index = 1; index < args.length; index++)
                {
                    File file = new File(args[index]);
                    Files filteredFiles = FilesUtils.search(file, Logar.LOGFILE_PATTERN).keepFileType().removeContaining("-anon.").sortByName();
                    source.addAll(filteredFiles);
                }
                Logar.anonymize(source);
            }
            else if (CLIUtils.isMatching(args, "archive", ".+", ".+"))
            {
                File source = new File(args[1]);
                File target = new File(args[2]);

                Logar.archive(source, target, OnOffOption.OFF);
            }
            else if (CLIUtils.isMatching(args, "archive", "-dry", ".+", ".+"))
            {
                File source = new File(args[2]);
                File target = new File(args[3]);

                Logar.archive(source, target, OnOffOption.ON);
            }
            else if (CLIUtils.isMatchingEllipsis(args, "check", ".+"))
            {
                Files source = new Files();
                for (int index = 1; index < args.length; index++)
                {
                    File file = new File(args[index]);
                    Files filteredFiles = FilesUtils.search(file, Logar.LOGFILE_PATTERN).keepFileType().sortByName();
                    source.addAll(filteredFiles);
                }
                Logar.checkLogFiles(source);
            }
            else if (CLIUtils.isMatchingEllipsis(args, "checksort", ".+"))
            {
                Files source = new Files();
                for (int index = 1; index < args.length; index++)
                {
                    File file = new File(args[index]);
                    Files filteredFiles = FilesUtils.search(file, Logar.LOGFILE_PATTERN).keepFileType().sortByName();
                    source.addAll(filteredFiles);
                }
                Logar.checkSort(source);
            }
            else if (CLIUtils.isMatching(args, "extract", "\\s*((ip)?(,)?(remoteuser)?(,)?(datetime)?(,)?(useragent)?)\\s*", "\\s*\\S+\\s*"))
            {
                ExtractOptions options = ExtractOptions.of(args[1]);
                File source = new File(args[2]);

                Logar.extract(source, options);
            }
            else if (CLIUtils.isMatchingEllipsis(args, "identify", ".+"))
            {
                Files source = new Files();
                for (int index = 1; index < args.length; index++)
                {
                    File file = new File(args[index]);
                    Files filteredFiles = FilesUtils.search(file, Logar.LOGFILE_PATTERN).keepFileType().sortByName();
                    source.addAll(filteredFiles);
                }
                Logar.identify(source);
            }
            else if (CLIUtils.isMatchingEllipsis(args, "sort", ".+"))
            {
                Files source = new Files();
                for (int index = 1; index < args.length; index++)
                {
                    File file = new File(args[index]);
                    Files filteredFiles = FilesUtils.search(file, Logar.LOGFILE_PATTERN).keepFileType().sortByName();
                    source.addAll(filteredFiles);
                }
                Logar.sort(source);
            }
            else if (CLIUtils.isMatchingEllipsis(args, "statuseragent", ".+"))
            {
                Files source = new Files();
                for (int index = 1; index < args.length; index++)
                {
                    File file = new File(args[index]);
                    Files filteredFiles = FilesUtils.search(file, Logar.LOGFILE_PATTERN).keepFileType().sortByName();
                    source.addAll(filteredFiles);
                }
                Logar.statUserAgents(source);
            }
            else if (CLIUtils.isMatching(args, "testconcate", ".+"))
            {
                File source = new File(args[1]);

                Logar.testConcate(source);
            }
            else
            {
                logger.info("Bad usage.");
                displayHelp();
            }
        }
        catch (Exception exception)
        {
            logger.error("Error: {}", exception.getMessage());
            exception.printStackTrace();
        }

        //
        logger.info("Done.");
    }
}
